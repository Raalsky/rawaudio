# Raw Audio Ethernet Communication

---

## Implementacja modemu akustycznego w języku Python

### Rafał Jankowski

*Repozytorium*: https://github.com/raalsky/rawaudio

Użyte dodatkowe biblioteki:

1. `numpy`
2. `wave`
3. `bitstring`
4. `tqdm`
5. `mkdocs`
6. `coverage`
7. `coverage-badge`

---

## Crème de la crème

1. Skrypt `setup.py` służący do zbudowania biblioteki dla Pythona.
2. Skonfigurowany *Travis CI* https://travis-ci.org/Raalsky/rawaudio do sprawdzania poprawności wykonania testów i budowania biblioteki.
3. Skrypt `coverage.sh` który sprawdza pokrycie kodu testami i generuje ładny przycisk na Githuba
4. **In Progress** dokumentacja biblioteki https://raalsky.github.io/rawaudio/

---

## Synchronizacja

Funkcja do synchronizacji `position_connection` znajduje się w `rawaudio/core/audio.py`

Funkcja pobiera dwukrotną długość sygnału z `pulseaudio`. Następnie sprawdzane są `alpha+1` fragmentów o długości jednego sygnału. Pozycja początku okna jest przesuwana o `signal length / alpha`. Dla każdego fragmentu obliczane jest `FFT` i na tej podstawie obliczana jest siła sygnału dla częstotliwości stanu wysokiego i niskiego `+/- freq_eps` w postaci sumy wartości na przedziale `[high - freq_eps, high + freq_eps]`,  analogicznie dla `low`.  Następnie obliczane są wartości `softmax_*` czyli sprowadzenie do procentowych udziałów wartości energii dla danych częstotliwości. Następnie obliczana jest różnica absolutna dwóch `softmaxów`. Na podstawie wszystkich różnic obliczane jest maksimum. Oznacza to, że znajdziemy takie przesunięcie, gdzie różnica w rozróżnianiu częstotliwości jest największa. Dodatkowo dla największej różnicy trzymane jest `force` obliczane na podstawie `+/- 20` największych wartości z fragmentu o największej różnicy. Jest to sprawdzane przed zwróceniem wyniku z doświadczalnie ustalonym `thresholdem` tzn. jeśli `force` jest mniejszy od pewnej wartości oznacza, że jest to szum. Funkcja zwraca `SOMETHING` lub `NOISE` w zależnośći od zawartości danego fragmentu.

---

## Testy

Wszystkie testy są oparte na standardowej bibliotece `unittest` . Sumaryczne pokrycie kodu źródłowego testami wynosi `82%` . Sprawdzane są takie elementy jak rozpoznawanie nasłuchiwanej częstotliwości, kodowanie i dekodowanie ramek ethernetowych i synchronizacja połączenia. 

*Pełna lista testów*:

1. `test_audio_utils.py` - sprawdzanie metody, która normalizuje amplitudę jaka została przesłana do funkcji, aby nie wychodziła poza przedział [0, 1]
2. `test_binary_frame.py` i `test_binary_utils.py` - testy sprawdzające metody generujące preambułę, kodowanie i dekodowanie 4B5B, kodowanie i dekodowanie takich elementów jak adres nadawcy, odbiorcy czy długość sygnału i testy sprawdzające poprawność kodowania i dekodowania pełnej ramki ethernetowej
3. `test_frequency_listening.py` - Sprawdzanie odczytywanych częstotliwości na podstawie wcześniej wygenerowanych plików `.wav` programem `Audacity`
4. `test_frequency_emitting.py` - Sprawdzanie generowanych częstotliwości przy zadanej długości sygnału i odczyt ich. Testy te są ograniczone przez rozmiar bufora w `pulseaudio` i prawdopodobnie uruchamianie odczytu i zapisu w osobnych wątkach znosi limit długości sygnału.
5. `test_position_fixing.py` - Sprawdzanie synchronizacji. Funkcja synchronizująca odczytuje 2 * długość sygnału i sprawdza czy w takim fragmencie jest coś co istotnie przypomina fragment preambuły. Jeżeli funkcji udało się dopasować to zwraca `SOMETHING` w przeciwnym przypadku (szum lub cisza) zwraca `NOISE`
6. `test_communication.py` - Testy operują na wcześniej wygenerowanych plikach `.wav` z wygenerowanymi ramkami ethernetowymi z różną zawartością i przy różnych prędkościach nadawczych. Sprawdzana jest synchronizacja i poprawność odczytanych danych. Testy są przygotowane dla sygnału o długościach `0.1s`, `0.08s`, `0.05s` i `0.025s`. 