# Raw Audio Ethernet Communication

[![Build Status](https://travis-ci.org/Raalsky/rawaudio.svg?branch=master)](https://travis-ci.org/Raalsky/rawaudio)
![Coverage](tests/coverage.svg)

Python library for low-level ethernet communication through audio driver.

## Documentation

The incomplete documentation for RawAudio is available [here](https://raalsky.github.io/rawaudio).

## Code Style

Keeping to a consistent code style throughout the project makes it easier to contribute and collaborate. Please stick to the guidelines in [PEP8](https://www.python.org/dev/peps/pep-0008) and the [Google Style Guide](https://google.github.io/styleguide/pyguide.html) unless there's a very good reason not to.
