# RawAudio Documentation

The source for RawAudio documentation is in this directory under `pages/`. 
Documentation uses extended Markdown, as implemented by [MkDocs](http://mkdocs.org).

## Building the documentation

- install MkDocs: `pip install mkdocs`
- `cd` to the `docs/` folder and run:
    - `mkdocs serve`    # Starts a local webserver:  [localhost:8000](localhost:8000)
    - `mkdocs build`    # Builds a static site in "site" directory
    
## Deploy the documentation

- `mkdocs gh-deploy`