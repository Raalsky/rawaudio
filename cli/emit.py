from rawaudio.controller import Controller
from rawaudio.emitters import AudioEmitter

AO = hex(55)
AN = hex(42)

con = Controller(emitter=AudioEmitter(time=0.1, high=880, low=440))
con.send(receiver=AN,
         transmitter=AO,
         data='Lorem ipsum dolor sit amet')
