import pulseaudio as pa
import numpy as np
import wave
import os

nchannels = 1
sampformat = pa.SAMPLE_S16LE
framerate = 44100
filename = 'rawaudio/samples/sample0025.wav'
length = 30.0

print(os.getcwd())

with pa.simple.open(direction=pa.STREAM_RECORD, format=sampformat, rate=framerate,
                    channels=nchannels) as recorder:
    wav = wave.open(filename, 'wb')
    wav.setnchannels(recorder.channels)
    wav.setsampwidth(recorder.sample_width)
    wav.setframerate(recorder.rate)
    nframes = int(length * recorder.rate)
    try:
        count = 0
        while count < nframes:
            data = recorder.read(min(100, nframes - count))
            count += len(data)
            wav.writeframes(data.astype(recorder.sample_type).tostring())
    finally:
        wav.close()
