from rawaudio.receivers import AudioReceiver
from rawaudio.binary.frame import EthernetFrame

if __name__ == '__main__':
    ar = AudioReceiver(time=0.1, high=880, low=440)

    for packet in ar:
        print(packet)
        fr = EthernetFrame()
        print(*fr.retrieve_packet(packet))
