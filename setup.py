from setuptools import setup, find_packages

setup(name='RawAudio',
      version='0.1.1',
      description='Python library for low-level ethernet communication through audio driver.',
      author='Rafal Jankowski',
      author_email='raalsky@gmail.com',
      url='https://github.com/Raalsky/rawaudio',
      packages=['pulseaudio', 'rawaudio', 'rawaudio.core', 'rawaudio.binary']
     )