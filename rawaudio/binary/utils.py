from binascii import crc32
import struct
from bitstring import BitArray


def pack48(data):
    return struct.pack(">Q", data)[-6:]


def unpack48(data):
    num = struct.unpack('>Q', b'\x00\x00' + data)
    return num


def unpack16(data):
    num = struct.unpack('>H', data)
    return num


def unpack32(data):
    return struct.unpack('>I', data)


def to_bin(data):
    return BitArray(data).bin


def to_bytes(data):
    return BitArray(data).bytes


def normalize_data(data):
    if type(data) is str:
        if data[:2] == '0x':
            data = BitArray(data)
        else:
            data = BitArray(data.encode('utf-8'))
    elif type(data) is int:
        data = BitArray(hex=hex(data))
    else:
        data = BitArray(data)
    return data


# Returns encoded sequence of bits
def encode_4b5b(data):
    bits_seq = data
    codes = {
        '0000': '11110',
        '0001': '01001',
        '0010': '10100',
        '0011': '10101',
        '0100': '01010',
        '0101': '01011',
        '0110': '01110',
        '0111': '01111',
        '1000': '10010',
        '1001': '10011',
        '1010': '10110',
        '1011': '10111',
        '1100': '11010',
        '1101': '11011',
        '1110': '11100',
        '1111': '11101'
    }
    out = ''
    for i in range(0, len(bits_seq), 4):
        out += codes[bits_seq[i: i + 4]]

    return BitArray(bin=out)


# Returns decoded sequence of bits from 4B5B encoding
def decode_4b5b(data):
    bits_seq = data
    codes = {
        '11110': '0000',
        '01001': '0001',
        '10100': '0010',
        '10101': '0011',
        '01010': '0100',
        '01011': '0101',
        '01110': '0110',
        '01111': '0111',
        '10010': '1000',
        '10011': '1001',
        '10110': '1010',
        '10111': '1011',
        '11010': '1100',
        '11011': '1101',
        '11100': '1110',
        '11101': '1111'
    }
    out = ''
    for i in range(0, len(bits_seq), 5):
        out += codes[bits_seq[i: i + 5]]

    return BitArray(bin=out)


def to_text(bit_array):
    return bit_array.decode('utf-8')
