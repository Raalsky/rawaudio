import struct
from .utils import *
from functools import partial


class Frame:
    @staticmethod
    def struct_preambula(data, transmitter, receiver, length=8):
        return BitArray(bytes(b'\xaa' * (length-1) + b'\xab'))

    @staticmethod
    def struct_receiver(data, transmitter, receiver, length=6):
        return BitArray(pack48(receiver.int))

    @staticmethod
    def struct_transmitter(data, transmitter, receiver, length=6):
        return BitArray(pack48(transmitter.int))

    @staticmethod
    def struct_length(data, transmitter, receiver, length=2):
        return BitArray(struct.pack('>H', len(data.bytes)))

    @staticmethod
    def struct_data(data, transmitter, receiver, length=-1):
        return BitArray(data)

    @staticmethod
    def struct_hash(data):
        # crc32 is in little-endian convert to big-endian
        crc = struct.pack('>L', crc32(data.bytes))
        return BitArray(crc)

    @staticmethod
    def struct_4b5b(data):
        return encode_4b5b(data)


class EthernetFrame(Frame):
    # Create processing structure
    def __init__(self):
        self.preambula = Frame.struct_preambula
        self.structure = [
            Frame.struct_receiver,
            Frame.struct_transmitter,
            Frame.struct_length,
            Frame.struct_data
        ]
        self.hasher = Frame.struct_hash
        self.encoder = Frame.struct_4b5b

    # Encode data with frame structure, returns sequence of bits
    def encode(self, data, transmitter, receiver):
        data = normalize_data(data)
        transmitter = normalize_data(transmitter)
        receiver = normalize_data(receiver)

        processed = sum([st_block(data, transmitter, receiver) for st_block in self.structure])
        processed_hash = self.hasher(processed)

        # processed_preambula = self.preambula(data, transmitter, receiver)
        # processed_encoded = self.encoder(sum([processed, processed_hash]))

        # print(Fore.BLUE + processed_preambula.bin, Fore.RED + processed.bin, Fore.GREEN + processed_hash.bin)
        # print(Fore.BLUE + processed_preambula.bin, Fore.YELLOW + processed_encoded.bin)
        # return sum([processed_preambula, processed_encoded]).bin

        return sum([processed, processed_hash]).bin

    def decode(self, frame):
        bytes = to_bytes(BitArray(bin=frame))

        if len(bytes) < 19:
            raise ValueError

        receiver_bytes = bytes[0:6]
        transmitter_bytes = bytes[6:12]
        length_bytes = bytes[12:14]
        data_bytes = bytes[14:-4]
        hash_bytes = bytes[-4:]

        # print(receiver_bytes, transmitter_bytes, length_bytes, data_bytes, hash_bytes)

        receiver = unpack48(receiver_bytes)[0]
        transmitter = unpack48(transmitter_bytes)[0]
        length = unpack16(length_bytes)[0]
        data = to_text(data_bytes)
        hash = unpack32(hash_bytes)

        if len(data_bytes) != length:
            raise ValueError

        encoded = self.encode(data, transmitter, receiver)

        assert encoded == frame

        # print(receiver, transmitter, length, data)

        return transmitter, receiver, data

    def create_packet(self, data, transmitter, receiver):
        message = self.encode(data, transmitter, receiver)
        preambula = self.preambula(data, transmitter, receiver).bin
        encoded = self.struct_4b5b(message).bin

        # print(message)

        return "".join([preambula, encoded])

    def retrieve_packet(self, packet):
        position = 2

        # Preambula
        #while packet[position-1] != packet[position-2]:
        #    position += 1

        # Data
        #data = packet[position:]
        data = packet

        # Problematic end
        end = len(data) % 5
        if end > 0:
            data = data[:-end]

        #print("RECEIVED:", end)
        #print(data)

        decoded_frame = decode_4b5b(data)
        #print(decoded_frame.bin)

        return self.decode(decoded_frame.bin)
