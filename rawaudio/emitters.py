from rawaudio.core.audio import AudioOutput


class AudioEmitter:
    def __init__(self, high=880, low=440, time=1, amplitude=0.8):
        self.HIGH = high
        self.LOW = low
        self.AMP = amplitude
        self.TIME = time

        try:
            self.driver = AudioOutput()
        except BrokenPipeError:
            print("Broken Pipe Error")

    def emit_bit(self, bit):
        bit = int(bit)
        if bit:
            self.driver.tone(self.HIGH, self.TIME, self.AMP)
        else:
            self.driver.tone(self.LOW, self.TIME, self.AMP)
