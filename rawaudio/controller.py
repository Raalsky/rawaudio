from rawaudio.binary.frame import EthernetFrame
from rawaudio.emitters import AudioEmitter
from rawaudio.receivers import AudioReceiver
from time import sleep
from tqdm import tqdm


class Controller:
    def __init__(self, frame=EthernetFrame(), emitter=AudioEmitter(), receiver=AudioReceiver(), pause=2):
        self.frame = frame
        self.emitter = emitter
        self.pause = pause
        self.receiver = receiver

    def send(self, receiver, transmitter, data):
        bites = self.frame.create_packet(data=data, receiver=receiver, transmitter=transmitter)

        #print(bites)

        for bit in tqdm(bites):
            self.emitter.emit_bit(bit)

        sleep(self.pause)

    def get(self):
        return self.receiver.get_bit()
