from rawaudio.core.audio import AudioInput
from enum import Enum
import numpy as np


class State(Enum):
    WAITING = 0
    PREAMBULA = 1
    DATA = 2


class AudioReceiver:
    def __init__(self, high=880, low=440, time=1, eps=50):
        self.HIGH = high
        self.LOW = low
        self.TIME = time
        self.EPS = eps

        try:
            self.driver = AudioInput()
        except BrokenPipeError:
            print("Broken Pipe Error")

    def get_bit(self):
        freq = self.driver.frequency(self.TIME)
        if np.abs(freq - self.HIGH) < self.EPS:
            return 1
        elif np.abs(freq - self.LOW) < self.EPS:
            return 0
        else:
            return -1

    def __iter__(self):
        try:
            while True:
                state = State.WAITING
                offset = self.driver.position_connection(signal_time=self.TIME, alpha=8, low=self.LOW, high=self.HIGH, freq_eps=10)

                if offset is not 'NOISE':
                    state = State.PREAMBULA
                    #print(state)
                    data = []
                    while True:
                        data.append(self.get_bit())
                        if data[-1] == -1:
                            yield "".join(map(str, data[:-1]))
                            break
                        if state == State.PREAMBULA and len(data) > 1 and data[-1] == 1 and data[-2] == 1:
                            data = []
                            state = State.DATA
                            #print(state)
        except KeyboardInterrupt:
            pass
