import time as tm
from math import sin, pi
import numpy as np
import pulseaudio as pa


def normalize_amplitude(amp):
    return (lambda x: min(1.0, max(0.0, x)))(amp)


class AudioOutput:
    def __init__(self, framerate=44100):
        self.FRAMERATE = framerate
        self.pipe = pa.simple.open(direction=pa.STREAM_PLAYBACK,
                                   format=pa.SAMPLE_S16LE,
                                   rate=self.FRAMERATE,
                                   channels=1)

    def __sinusoide__(self, frequency, amplitude, time):
        amplitude = normalize_amplitude(amplitude)
        sinusoide = (2 ** 15) * amplitude * np.array(
            [(np.sin(2 * np.pi * np.arange(self.FRAMERATE*time) * frequency / self.FRAMERATE)).astype(np.float32)])
        return sinusoide

    def tone(self, frequency=440, time=-1, amplitude=0.8):
        self.pipe.write(np.array(self.__sinusoide__(frequency, amplitude, time)))

    def flush(self):
        self.pipe.flush()


class AudioInput:
    def __init__(self, framerate=44100):
        self.FRAMERATE = framerate
        self.pipe = pa.simple.open(direction=pa.STREAM_RECORD,
                                   format=pa.SAMPLE_S16LE,
                                   rate=self.FRAMERATE,
                                   channels=1)

    def flush(self):
        self.pipe.flush()

    def listen(self, time):
        samples = self.pipe.read(int(time * self.FRAMERATE))
        data = np.fft.fft(samples)
        data = data[0:len(data)//2]
        data = np.abs(data)

        return data

    def frequency(self, time):
        data = self.listen(time)
        freq = int(np.argmax(data) / time)
        return freq

    def position_connection(self, signal_time=0.1, alpha=4, low=440, high=880, freq_eps=15):
        # Read 2 * signal_time samples
        signal_samples_len = int(signal_time * self.FRAMERATE)
        samples_count_to_load = 2 * signal_samples_len
        samples = self.pipe.read(samples_count_to_load)
        force_for_max_abs = 0
        offset_for_max_abs = 0
        abs_diff_for_max = 1.0
        offset_portion_length = int(signal_time * self.FRAMERATE / alpha)

        # Noise detection: if first 0.4 samples are marginal (not certain frequency) ...
        data_1_4 = samples[0: int(0.4 * signal_samples_len)]
        data_1_4 = np.fft.fft(data_1_4)
        data_1_4 = np.abs(data_1_4[0: len(data_1_4) // 2])
        if np.max(data_1_4) < 10.0:
            return 'NOISE'

        # 1/alpha , 2/alpha, 3/alpha, ...
        for begin in range(alpha + 1):
            offset = begin * offset_portion_length
            offset_samples = samples[offset: offset + signal_samples_len]
            values = np.fft.fft(offset_samples)
            values = np.abs(values[0: len(values) // 2])

            if np.abs(int(np.argmax(values) / signal_time) - 880) > freq_eps and np.abs(int(np.argmax(values) / signal_time) - 440) > freq_eps:
                return 'NOISE'

            max_value = np.sum(values[np.argmax(values)-10: np.argmax(values)+10])
            force = 100 * max_value / np.sum(values)
            high_value = np.sum(values[int((high - freq_eps) * signal_time): int((high + freq_eps) * signal_time)])
            low_value = np.sum(values[int((low - freq_eps) * signal_time): int((low + freq_eps) * signal_time)])

            # Softmax
            high_softmax = high_value / (high_value + low_value)
            low_softmax = low_value / (high_value + low_value)

            abs_diff = np.abs(high_softmax - low_softmax)

            if abs_diff < abs_diff_for_max:
                 abs_diff_for_max = abs_diff
                 offset_for_max_abs = offset
                 force_for_max_abs = force
        if force_for_max_abs < 8:
            return 'NOISE'
        else:
            if offset_for_max_abs != 0:
                samples = self.pipe.read(offset_for_max_abs)
            return 'SOMETHING'

