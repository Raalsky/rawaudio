def normalize_amplitude(amp):
    return (lambda x: min(1.0, max(0.0, x)))(amp)
