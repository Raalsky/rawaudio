#!/usr/bin/env bash
coverage run --source=rawaudio/ -m unittest discover -s tests/
coverage report -m
rm -rf tests/coverage.svg
coverage-badge -o tests/coverage.svg