from rawaudio.core.utils import normalize_amplitude
import unittest


class TestAudioUtils(unittest.TestCase):
    def test_amp_norm_1(self):
        amp = normalize_amplitude(0.0)
        self.assertEqual(amp, 0.0)

    def test_amp_norm_2(self):
        amp = normalize_amplitude(1.0)
        self.assertEqual(amp, 1.0)

    def test_amp_norm_3(self):
        amp = normalize_amplitude(0.5)
        self.assertEqual(amp, 0.5)

    def test_amp_norm_4(self):
        amp = normalize_amplitude(1.5)
        self.assertEqual(amp, 1.0)

    def test_amp_norm_5(self):
        amp = normalize_amplitude(-0.5)
        self.assertEqual(amp, 0.0)


if __name__ == '__main__':
    unittest.main()
