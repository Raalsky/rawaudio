from rawaudio.binary.frame import Frame, EthernetFrame
from rawaudio.binary.utils import *
from bitstring import BitArray
import unittest


class TestAudioUtils(unittest.TestCase):
    def test_frame_preambula_1(self):
        data = Frame.struct_preambula(None, None, None, length=1)
        self.assertEqual(data.bin, '10101011')

    def test_frame_preambula_2(self):
        data = Frame.struct_preambula(None, None, None, length=2)
        self.assertEqual(data.bin, '1010101010101011')

    def test_frame_preambula_3(self):
        data = Frame.struct_preambula(None, None, None, length=8)
        self.assertEqual(data.bin, '1010101010101010101010101010101010101010101010101010101010101011')

    def test_frame_receiver_1(self):
        receiver = normalize_data(hex(60))
        data = Frame.struct_receiver(None, None, receiver, None)
        self.assertEqual(data.bin, '000000000000000000000000000000000000000000111100')

    def test_frame_transmitter_1(self):
        transmitter = normalize_data(60)
        data = Frame.struct_transmitter(None, transmitter, None, None)
        self.assertEqual(data.bin, '000000000000000000000000000000000000000000111100')

    def test_frame_length_1(self):
        data = normalize_data('0xdeadbeef')
        data = Frame.struct_length(data, None, None, None)
        self.assertEqual(data.bin, '0000000000000100')

    def test_frame_4b5b_encode_1(self):
        data = normalize_data('a b')
        encoded = Frame.struct_4b5b(data.bin)
        self.assertEqual(to_bin(encoded), '011100100110100111100111010100')

    def test_frame_encode_1(self):
        transmitter = normalize_data(hex(1))
        receiver = normalize_data(hex(2))
        data = normalize_data('a b')
        fr = EthernetFrame()
        encoded = fr.encode(data, transmitter, receiver)
        self.assertEqual(encoded, '000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000001000000000000001101100001001000000110001010000011000100001001011010101000')

    def test_frame_to_bytes_1(self):
        encoded = to_bytes(BitArray(hex='0xdeadbeef'))
        self.assertEqual(encoded, b'\xde\xad\xbe\xef')

    def test_to_text_1(self):
        text = to_text(b'a b')
        self.assertEqual(text, 'a b')

    def test_frame_encode_decode_1(self):
        transmitter = 1
        receiver = 2
        data = 'a b'

        norm_transmitter = normalize_data(hex(transmitter))
        norm_receiver = normalize_data(hex(receiver))
        norm_data = normalize_data(data)
        fr = EthernetFrame()
        encoded = fr.encode(norm_data, norm_transmitter, norm_receiver)
        decoded_transmitter, decoded_receiver, decoded_data = fr.decode(encoded)
        self.assertEqual([transmitter, receiver, data], [decoded_transmitter, decoded_receiver, decoded_data])

    def test_frame_encode_decode_2(self):
        transmitter = 63
        receiver = 62
        data = 'a b'

        norm_transmitter = normalize_data(hex(transmitter))
        norm_receiver = normalize_data(hex(receiver))
        norm_data = normalize_data(data)
        fr = EthernetFrame()
        encoded = fr.encode(norm_data, norm_transmitter, norm_receiver)
        decoded_transmitter, decoded_receiver, decoded_data = fr.decode(encoded)
        self.assertEqual([transmitter, receiver, data], [decoded_transmitter, decoded_receiver, decoded_data])

    def test_frame_encode_decode_3(self):
        transmitter = 64
        receiver = 63
        data = 'Lorem ipsum dolor sit amet'

        norm_transmitter = normalize_data(hex(transmitter))
        norm_receiver = normalize_data(hex(receiver))
        norm_data = normalize_data(data)
        fr = EthernetFrame()
        encoded = fr.encode(norm_data, norm_transmitter, norm_receiver)
        decoded_transmitter, decoded_receiver, decoded_data = fr.decode(encoded)
        self.assertEqual([transmitter, receiver, data], [decoded_transmitter, decoded_receiver, decoded_data])

    def test_frame_encode_decode_4(self):
        transmitter = 64
        receiver = 0
        data = 'Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię trzeba cenić, ten tylko się strony obie Tadeusz przyglądał się biję, tego dnia powiadał. Dobrze, mój Tadeuszu bo tak nazywano młodzieńca, który teraz jeśli zechcesz, i czuł, że w nié dzwonił, znak dawał, że mi w domu dawne obyczaje wtenczas wszyscy siedli i znowu jak śnieg biała gdzie panieńskim rumieńcem dzięcielina pała a oni tak nazywano młodzieńca, który nosił Kościuszkowskie miano na skarb carski zabierano. Czasem do których później odkładać goście głodni, chodzili daleko na które na spoczynek powraca.'

        norm_transmitter = normalize_data(hex(transmitter))
        norm_receiver = normalize_data(hex(receiver))
        norm_data = normalize_data(data)
        fr = EthernetFrame()
        encoded = fr.encode(norm_data, norm_transmitter, norm_receiver)
        decoded_transmitter, decoded_receiver, decoded_data = fr.decode(encoded)
        self.assertEqual([transmitter, receiver, data], [decoded_transmitter, decoded_receiver, decoded_data])

    def test_frame_encode_decode_5(self):
        transmitter = 64
        receiver = 0
        data = ''

        norm_transmitter = normalize_data(hex(transmitter))
        norm_receiver = normalize_data(hex(receiver))
        norm_data = normalize_data(data)
        fr = EthernetFrame()
        encoded = fr.encode(norm_data, norm_transmitter, norm_receiver)
        self.assertRaises(ValueError, fr.decode, encoded)

    def test_frame_encode_decode_6(self):
        transmitter = 64
        receiver = 63
        data = 'a b'

        norm_transmitter = normalize_data(hex(transmitter))
        norm_receiver = normalize_data(hex(receiver))
        norm_data = normalize_data(data)
        fr = EthernetFrame()
        encoded = fr.encode(norm_data, norm_transmitter, norm_receiver)
        encoded = encoded[:6*8+6*8+1] + '1' + encoded[6*8+6*8+2:]
        self.assertRaises(ValueError, fr.decode, encoded)


if __name__ == '__main__':
    unittest.main()
