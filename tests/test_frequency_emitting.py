from rawaudio.core.audio import AudioInput, AudioOutput
import unittest
import os
import pulseaudio


class TestFrequencyEmit(unittest.TestCase):
    def test_emit_440_1s(self):
        if '__PULSEAUDIO_WAVFILE__' in os.environ:
            os.environ.pop('__PULSEAUDIO_WAVFILE__')
        a_in = AudioInput()
        a_out = AudioOutput()

        a_in.flush()
        a_out.flush()

        a_out.tone(440, 1, 0.8)
        freq = a_in.frequency(1)

        a_in.flush()
        a_out.flush()

        self.assertEqual(440, freq)

    def test_emit_880_1s(self):
        if '__PULSEAUDIO_WAVFILE__' in os.environ:
            os.environ.pop('__PULSEAUDIO_WAVFILE__')
        a_in = AudioInput()
        a_out = AudioOutput()

        a_in.flush()
        a_out.flush()

        a_out.tone(880, 1, 0.8)
        freq = a_in.frequency(1)

        a_in.flush()
        a_out.flush()

        self.assertEqual(880, freq)

    def test_emit_9800_1s(self):
        if '__PULSEAUDIO_WAVFILE__' in os.environ:
            os.environ.pop('__PULSEAUDIO_WAVFILE__')
        a_in = AudioInput()
        a_out = AudioOutput()

        a_in.flush()
        a_out.flush()

        a_out.tone(9800, 1, 0.8)
        freq = a_in.frequency(1)

        a_in.flush()
        a_out.flush()

        self.assertEqual(9800, freq)


if __name__ == '__main__':
    unittest.main()
