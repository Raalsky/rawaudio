from rawaudio.binary.utils import *
import unittest
from bitstring import BitArray


class TestBinaryUtils(unittest.TestCase):
    def test_encode_4B5B_1(self):
        out = encode_4b5b(BitArray('0x0').bin).bin
        self.assertEqual(out, '11110')

    def test_encode_4B5B_2(self):
        out = encode_4b5b(BitArray('0xa').bin).bin
        self.assertEqual(out, '10110')

    def test_encode_4B5B_3(self):
        out = encode_4b5b(BitArray('0xf0').bin).bin
        self.assertEqual(out, '1110111110')

    def test_encode_4B5B_decode_1(self):
        out = decode_4b5b(encode_4b5b(BitArray('0xf0').bin).bin).bytes
        self.assertEqual(out, b'\xf0')

    def test_encode_4B5B_decode_2(self):
        out = decode_4b5b(encode_4b5b(BitArray('0xa0').bin).bin).bytes
        self.assertEqual(out, b'\xa0')

    def test_encode_4B5B_decode_3(self):
        out = decode_4b5b(encode_4b5b(BitArray('0x00').bin).bin).bytes
        self.assertEqual(out, b'\x00')

    def test_convert_1(self):
        out = to_bin('0xdeadbeef')
        self.assertEqual(out, '11011110101011011011111011101111')

if __name__ == '__main__':
    unittest.main()
