from rawaudio.receivers import AudioReceiver
from rawaudio.binary.frame import EthernetFrame
import unittest
import pulseaudio as pa
import wave
import os
from rawaudio.controller import Controller
from rawaudio.emitters import AudioEmitter


class TestConnection(unittest.TestCase):
    def test_connection_01(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/sample01.wav'
        ar = AudioReceiver(time=0.1, high=880, low=440)
        fr = EthernetFrame()

        for packet in ar:
            transmitter, receiver, data = fr.retrieve_packet(packet)
            self.assertEqual([0, 1, 'a'], [transmitter, receiver, data])
            break

    def test_connection_008(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/sample008.wav'
        ar = AudioReceiver(time=0.08, high=880, low=440)
        fr = EthernetFrame()

        for packet in ar:
            transmitter, receiver, data = fr.retrieve_packet(packet)
            self.assertEqual([transmitter, receiver, data], [0, 1, 'a'])
            break

    def test_connection_005(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/sample005.wav'
        ar = AudioReceiver(time=0.05, high=880, low=440)
        fr = EthernetFrame()

        for packet in ar:
            transmitter, receiver, data = fr.retrieve_packet(packet)
            self.assertEqual([transmitter, receiver, data], [0, 1, 'a'])
            break

    def test_connection_0025(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/sample0025.wav'
        ar = AudioReceiver(time=0.025, high=880, low=440)
        fr = EthernetFrame()

        for packet in ar:
            transmitter, receiver, data = fr.retrieve_packet(packet)
            self.assertEqual([transmitter, receiver, data], [0, 1, 'Lorem ipsum dolor sit amet'])
            break

    # def test_connection_002(self):
    #     os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/sample002.wav'
    #     ar = AudioReceiver(time=0.02, high=880, low=440)
    #     fr = EthernetFrame()
    #
    #     for packet in ar:
    #         transmitter, receiver, data = fr.retrieve_packet(packet)
    #         self.assertEqual([transmitter, receiver, data], [0, 1, 'a'])
    #         break


if __name__ == '__main__':
    unittest.main()