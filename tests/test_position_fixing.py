from rawaudio.core.audio import AudioInput, AudioOutput
import unittest
import os
import pulseaudio


class TestPositionFixing(unittest.TestCase):
    def test_file_0(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/0.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('SOMETHING', offset)

    def test_file_1(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/1.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)
        
        self.assertEqual('SOMETHING', offset)

    def test_file_2(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/2.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('SOMETHING', offset)

    def test_file_3(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/3.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('SOMETHING', offset)

    def test_file_4(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/4.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('NOISE', offset)

    def test_file_5(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/5.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('NOISE', offset)

    def test_file_6(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/6.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('NOISE', offset)

    def test_file_7(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/7.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('NOISE', offset)

    def test_file_8(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/8.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('NOISE', offset)

    def test_file_9(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/9.wav'

        a = AudioInput()
        offset = a.position_connection(0.1)

        self.assertEqual('NOISE', offset)


if __name__ == '__main__':
    unittest.main()
