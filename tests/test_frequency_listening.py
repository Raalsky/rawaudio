from rawaudio.core.audio import AudioInput, AudioOutput
import unittest
import os
import pulseaudio


class TestFrequencyListen(unittest.TestCase):
    def test_tone_880_1s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq880.wav'

        a = AudioInput()
        freq = a.frequency(1)
        
        self.assertEqual(freq, 880)

    def test_tone_880_0_1s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq880.wav'

        a = AudioInput()
        freq = a.frequency(0.1)

        self.assertEqual(freq, 880)

    def test_tone_880_0_5s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq880.wav'

        a = AudioInput()
        freq = a.frequency(0.5)

        self.assertEqual(freq, 880)

    def test_tone_440_1s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq440.wav'

        a = AudioInput()
        freq = a.frequency(1)

        self.assertEqual(freq, 440)

    def test_tone_440_0_1s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq440.wav'

        a = AudioInput()
        freq = a.frequency(0.1)

        self.assertEqual(freq, 440)

    def test_tone_440_0_5s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq440.wav'

        a = AudioInput()
        freq = a.frequency(0.5)

        self.assertEqual(freq, 440)

    def test_tone_1024_1s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq1024.wav'

        a = AudioInput()
        freq = a.frequency(1)

        self.assertEqual(freq, 1024)

    def test_tone_1024_0_5s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq1024.wav'

        a = AudioInput()
        freq = a.frequency(0.5)

        self.assertEqual(freq, 1024)
        
    def test_tone_2048_1s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq2048.wav'

        a = AudioInput()
        freq = a.frequency(1)

        self.assertEqual(freq, 2048)

    def test_tone_2048_0_5s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq2048.wav'

        a = AudioInput()
        freq = a.frequency(0.5)

        self.assertEqual(freq, 2048)
        
    def test_tone_9800_1s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq9800.wav'

        a = AudioInput()
        freq = a.frequency(1)

        self.assertEqual(freq, 9800)

    def test_tone_9800_0_5s(self):
        os.environ['__PULSEAUDIO_WAVFILE__'] = 'samples/freq9800.wav'

        a = AudioInput()
        freq = a.frequency(0.5)

        self.assertEqual(freq, 9800)


if __name__ == '__main__':
    unittest.main()
